This repo holds my docker container for my plex server.
I added the docker-compose.override.yml to my .gitignore since it holds my Plex Server API key.
This is linked to Traefik in another container. I will need to add Authelia capability to this container
and add kubernetes capability as well.
